<?php
//! Permet l'affichage des erreurs - A ne pas commit
error_reporting(-1);

// J'intègre obligatoirement (une fois) le contenu de mon fichier de connexion à ma bdd
require_once("../utils/db_connect.php");
// J'intègre obligatoirement le contenu de mon fichier de fonctions
require("../utils/function.php");

// J'appelle ma fonction pour savoir si mon utilisateur est connecté
isConnected();
// J'appelle ma fonction pour savoir si mon utilisateur est admin
isAdmin();

//? Si ma méthode de requête est POST alors j'affecte à ma variable $method le contenu de la superglobale $_POST
if ($_SERVER["REQUEST_METHOD"] == "POST") $method = $_POST;
//? Sinon j'affecte à ma variable $method le contenu de la superglobale $_GET
else $method = $_GET;

switch ($method["choice"]) {
    case 'select':
        // Je récupère toutes les catégories
        $req = $db->query("SELECT * FROM categories");

        // J'affecte la totalité de mes résultats à la variable $categories
        $categories = $req->fetchAll(PDO::FETCH_ASSOC);

        // J'envoie une réponse avec un success true ainsi que les catégories
        echo json_encode(["success" => true, "categories" => $categories]);
        break;

    case 'select_id':
        if (!isset($method["id"]) || empty(trim($method["id"]))) {
            // J'envoie une réponse avec un success false et un message d'erreur
            echo json_encode(["success" => false, "error" => "Id manquant"]);
            die; //! J'arrête l'exécution du script
        }

        // Je récupère la catégorie ciblée par l'id en paramètre
        $req = $db->prepare("SELECT * FROM categories WHERE id = ?");
        $req->execute([$method["id"]]);

        // J'affecte à ma variable $category le résultat unique (ou pas de résultat) de ma requete SQL
        $category = $req->fetch(PDO::FETCH_ASSOC);

        // J'envoie une réponse avec un success true ainsi que la category
        echo json_encode(["success" => true, "category" => $category]);

        break;

    case 'insert':
        //? Si la requête HTTP n'est pas en POST alors
        if ($_SERVER["REQUEST_METHOD"] != "POST") {
            // J'envoie une réponse avec un success false et un message d'erreur
            echo json_encode(["success" => false, "error" => "Mauvaise méthode"]);
            die; //! J'arrête l'exécution du script
        }

        //? Si je n'ai pas le paramètre "name" OU s'il est vide alors
        if (!isset($method["name"]) || empty(trim($method["name"]))) {
            // J'envoie une réponse avec un success false et un message d'erreur
            echo json_encode(["success" => false, "error" => "Données manquantes"]);
            die; //! J'arrête l'exécution du script
        }

        // J'écris une requete préparée d'insertion de mes données dans la table categories
        $req = $db->prepare("INSERT INTO categories(name) VALUES (?)");
        $req->execute([$method["name"]]);

        // J'envoie une réponse avec un success true
        echo json_encode(["success" => true]);

        break;

    case 'update':
        //? Si ma requête HTTP n'est pas en POST alors
        if ($_SERVER["REQUEST_METHOD"] != "POST") {
            // J'envoie une réponse avec un success false et un message d'erreur
            echo json_encode(["success" => false, "error" => "Mauvaise méthode"]);
            die; //! J'arrête l'exécution du script
        }

        //? Si je n'ai pas les paramètres "name" et "id" OU qu'ils sont vides alors
        if (!isset($method["name"], $method["id"]) || empty(trim($method["name"])) || empty(trim($method["id"]))) {
            // J'envoie une réponse avec un success false et un message d'erreur
            echo json_encode(["success" => false, "error" => "Données manquantes"]);
            die; //! J'arrête l'exécution du script
        }

        // J'écris une requete préparée de mise à jour de la categorie
        $req = $db->prepare("UPDATE categories SET name = :name WHERE id = :id");
        // J'affecte à chaque clé les valeurs correspondantes grâce au bindValue
        $req->bindValue(":name", $method["name"]);
        $req->bindValue(":id", $method["id"]);
        $req->execute();

        //? Si j'ai 1 résultat avec c'est un succès
        if ($req->rowCount()) echo json_encode(["success" => true]);
        //? Sinon
        else echo json_encode(["success" => false, "error" => "Erreur lors de la mise à jour"]);

        break;

    case 'delete':
        //? Si ma requête HTTP n'est pas en POST alors
        if ($_SERVER["REQUEST_METHOD"] != "POST") {
            // J'envoie une réponse avec un success false et un message d'erreur
            echo json_encode(["success" => false, "error" => "Mauvaise méthode"]);
            die; //! J'arrête l'exécution du script
        }

        //? Si je n'ai pas de paramètre "id" OU qu'il est vide alors
        if (!isset($method["id"]) || empty(trim($method["id"]))) {
            // J'envoie une réponse avec un success false et un message d'erreur
            echo json_encode(["success" => false, "error" => "Id manquant"]);
            die; //! J'arrête l'exécution du script
        }

        // J'écris une requete préparée de suppression de la catégorie
        $req = $db->prepare("DELETE FROM categories WHERE id = ?");
        $req->execute([$method["id"]]);

        //? Si j'ai 1 résultat avec c'est un succès
        if ($req->rowCount()) echo json_encode(["success" => true]);
        //? Sinon
        else echo json_encode(["success" => false, "error" => "Erreur lors de la suppression"]);

        break;

    default:
        //! Aucune case ne correspond à mon choix
        // J'envoie une réponse avec un success false et un message d'erreur
        echo json_encode(["success" => false, "error" => "Ce choix n'existe pas"]);
        break;
}
