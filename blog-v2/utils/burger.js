$("aside p").text(user.firstname + " " + user.lastname);

$(".fa-bars").click(() => {
    $("aside").addClass("open");
    $("#overlay").addClass("open");
});

$("#overlay").click(() => {
    $("aside").removeClass("open");
    $("#overlay").removeClass("open");
});

//? Si l'utilisateur n'est pas administrateur je supprime le menu administrateur
if (user.admin == 0) $("#admin_menu").remove();